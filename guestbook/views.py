from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate

from .models import Message
from .forms import MessageForm

def index(request):
    if request.method == 'POST' and request.user.is_authenticated:
        message_form = MessageForm(request.POST)
        if message_form.is_valid():
            new_message = message_form.save(commit=False)
            new_message.user = request.user
            new_message.save()
            return redirect('index')

    messages = Message.objects.order_by('-pub_date')
    context = {'messages': messages}
    if request.user.is_authenticated:
        context['message_form'] = MessageForm()
    return render(request, 'guestbook/index.html', context)


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('index')
        else:
            return render(request, 'guestbook/signup.html', {'form': form})
    else:
        form = UserCreationForm()
    return render(request, 'guestbook/signup.html', {'form': form})
