from django.forms import ModelForm, Textarea
from .models import Message

class MessageForm(ModelForm):
    class Meta:
        model = Message
        fields = ['message_text'] 
        widgets = {
                'message_text': Textarea(attrs={'cols':'', 'rows':'' ,'style':'width:100%;', 'placeholder':'Type your message text here.'}),
                }
