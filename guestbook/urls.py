from django.conf.urls import url
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login/$', auth_views.LoginView.as_view(template_name='guestbook/login.html'), name='login'),
    url(r'^logout/$', auth_views.LogoutView.as_view(next_page='index'), name='logout'),
    url(r'^signup/$', views.signup, name='signup'),
]

