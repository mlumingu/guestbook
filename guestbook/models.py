from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

class Message(models.Model):
    message_text = models.TextField()
    pub_date = models.DateTimeField(default=datetime.now)
    user = models.ForeignKey(User)
    def __str__(self):
        return self.message_text[:20]
